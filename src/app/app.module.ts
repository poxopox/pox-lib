import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { InputComponent } from './lib/input/input.component';
import { SelectComponent } from './lib/select/select.component';
import { ButtonComponent } from './lib/button/button.component';
import { TableComponent } from './lib/table/table.component';
import { CardComponent } from './lib/card/card.component';
import { TextComponent } from './lib/text/text.component';
import { DropdownComponent } from './lib/dropdown/dropdown.component';

@NgModule({
  declarations: [
    AppComponent,
    InputComponent,
    SelectComponent,
    ButtonComponent,
    TableComponent,
    CardComponent,
    TextComponent,
    DropdownComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
